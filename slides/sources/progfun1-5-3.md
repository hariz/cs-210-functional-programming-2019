% Implicit Parameters
%
%

Making Sort more General
========================

Problem: How to parameterize `msort` so that it can also be used for
lists with elements other than `Int`?

      def msort[T](xs: List[T]): List[T] = ...

does not work, because the comparison `<` in `merge` is not defined
for arbitrary types `T`.

\red{Idea:} Parameterize `merge` with the necessary comparison function.

Parameterization of Sort
========================

The most flexible design is to make the function \verb@sort@
polymorphic and to pass the comparison operation as an additional
parameter:

      def msort[T](xs: List[T])(lt: (T, T) => Boolean) =
        ...
          merge(msort(fst)(lt), msort(snd)(lt))

Merge then needs to be adapted as follows:

      def merge(xs: List[T], ys: List[T]) = (xs, ys) match
        ...
        case (x :: xs1, y :: ys1) =>
          if lt(x, y) then ...
          else ...

Calling Parameterized Sort
==========================

We can now call `msort` as follows:

      val xs = List(-5, 6, 3, 2, 7)
      val fruits = List("apple", "pear", "orange", "pineapple")

      msort(xs)((x: Int, y: Int) => x < y)
      msort(fruits)((x: String, y: String) => x.compareTo(y) < 0)

Or, since parameter types can be inferred from the call `msort(xs)`:

      msort(xs)((x, y) => x < y)


Parametrization with Ordering
=============================

There is already a class in the standard library that represents orderings.

      scala.math.Ordering[T]

provides ways to compare elements of type `T`. So instead of
parameterizing with the `lt` operation directly, we could parameterize
with `Ordering` instead:

      def msort[T](xs: List[T])(ord: Ordering[T]) =

        def merge(xs: List[T], ys: List[T]) =
          ... if ord.lt(x, y) then ...

        ... merge(msort(fst)(ord), msort(snd)(ord)) ...

Ordering Instances:
===================

Calling the new `msort` can be done like this:

      import math.Ordering

      msort(nums)(Ordering.Int)
      msort(fruits)(Ordering.String)

This makes use of the values `Int` and `String` defined in the
`scala.math.Ordering` object, which produce the right orderings on
integers and strings.

Aside: Implicit Parameters
==========================

\red{Problem:} Passing around `lt` or `ord` values is cumbersome.

We can avoid this by making `ord` an implicit parameter.

      def msort[T](xs: List[T])(implicit ord: Ordering[T]) =

        def merge(xs: List[T], ys: List[T]) =
          ... if ord.lt(x, y) then ...

        ... merge(msort(fst), msort(snd)) ...

Then calls to `msort` can avoid the ordering parameters:

      msort(nums)
      msort(fruits)

The compiler will figure out the right implicit to pass based on the
demanded type.

Rules for Implicit Parameters
=============================

Say, a function takes an implicit parameter of type `T`.

The compiler will search an implicit definition that

 - is marked `implicit`
 - has a type compatible with `T`
 - is visible at the point of the function call, or is defined
   in a companion object associated with `T`.

If there is a single (most specific) definition, it will be taken as
actual argument for the implicit parameter.

Otherwise it's an error.

Exercise: Implicit Parameters
=============================

Consider the following line of the definition of `msort`:

        ... merge(msort(fst), msort(snd)) ...

Which implicit argument is inserted?

      O        Ordering.Int
      O        Ordering.String
      O        the "ord" parameter of "msort"















