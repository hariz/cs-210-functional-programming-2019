% Evaluation and Operators
%
%

Classes and Substitutions
=========================

We previously defined the meaning of a function application using
a computation model based on substitution. Now we extend this
model to classes and objects.

\red{Question:} How is an instantiation of the class
$\btt C(e_1, ..., e_m)$ evaluted?

\red{Answer:} The expression arguments $\btt e_1, ..., e_m$
are evaluated like the arguments of a normal function. That's it.

The resulting expression, say, $\btt C(v_1, ..., v_m)$, is
already a value.

Classes and Substitutions
=========================

Now suppose that we have a class definition,

$$\btt
      class\ C(x_1, ..., x_m) \{\ ...\ def\ f(y_1, ..., y_n) = b\ ...\ \}
$$
where

 - The formal parameters of the class are $\btt x_1, ..., x_m$.
 - The class defines a method $\btt f$ with formal parameters
   $\btt y_1, ..., y_n$.

(The list of function parameters can be absent. For simplicity, we
have omitted the parameter types.)

\red{Question:} How is the following expression evaluated?
$$\btt
        C(v_1, ..., v_m).f(w_1, ..., w_n)
$$

Classes and Substitutions (2)
=============================

\red{Answer:} The expression $\btt C(v_1, ..., v_m).f(w_1, ..., w_n)$ is rewritten to:
$$\btt
[w_1/y_1, ..., w_n/y_n]
  [v_1/x_1, ..., v_m/x_m]
    [C(v_1, ..., v_m)/this]\,b
$$
There are three substitutions at work here:

 - the substitution of the formal parameters $\btt y_1, ..., y_n$ of the function $\btt f$ by the
arguments $\btt w_1, ..., w_n$,
 - the substitution of the formal parameters $\btt x_1, ..., x_m$ of the class $\btt C$ by the class arguments
$\btt v_1, ..., v_m$,
 - the substitution of the self reference $this$ by the value of the object $\btt C(v_1, ..., v_n)$.

Object Rewriting Examples
=========================

   `      Rational(1, 2).numer`
->
   $\rightarrow$  $\btt [1/x, 2/y]\ []\ [Rational(1, 2)/this]$ `x`
->
   =$\,$  `1`
\medskip
->
  `      Rational(1, 2).less(Rational(2, 3))`
->
   $\rightarrow$ $\btt [1/x, 2/y]\ [Rational(2, 3)/that]\ [Rational(1, 2)/this]$

\nl\gap `this.numer * that.denom < that.numer * this.denom`
->
   =$\,$  `Rational(1, 2).numer * Rational(2, 3).denom <`

\nl\gap `Rational(2, 3).numer * Rational(1, 2).denom`
->
   $\rightarrow\dhd$  `1 * 3 < 2 * 2`

   $\rightarrow\dhd$  `true`


Operators
=========

In principle, the rational numbers defined by `Rational` are
as natural as integers.

But for the user of these abstractions, there is a noticeable
difference:

 - We write `x + y`, if `x` and `y` are integers, but
 - We write `r.add(s)` if `r` and `s` are rational numbers.

In Scala, we can eliminate this difference. We proceed in two steps.

Step 1: Relaxed Identifiers
===========================

Operators such as `+` or `<` count as identifiers in Scala.

Thus, an identifier can be:

 - \red{Alphanumeric}: starting with a letter, followed by a sequence of letters or numbers
 - \red{Symbolic}: starting with an operator symbol, followed by other operator symbols.
 - The underscore character `'_'` counts as a letter.
 - Alphanumeric identifiers can also end in an underscore, followed by some operator symbols.

Examples of identifiers:

      x1     *     +?%&     vector_++     counter_=


Step 1: Relaxed Identifiers
===========================

Since operators are identifiers, it is possible to use them as method names. E.g.

    class Rational {
      def + (x: Rational): Rational = ...
      def < (x: Rational): Rational = ...
    }

Step 2: Infix Notation
======================

An operator method with a single parameter can be used as an infix operator.

A normal method with a single parameter can also be used as an infix operator if it
is declared @infix. E.g.

    class Rational {
      @infix def max(that Rational): Rational = ...
    }

It is therefore possible to write

      r + s                                           r.+(s)
      r < s             /* in place of */             r.<(s)
      r max s                                         r.max(s)


Operators for Rationals
=======================

A more natural definition of class `Rational`:

      class Rational(x: Int, y: Int) {
        private def gcd(a: Int, b: Int): Int =
          if b == 0 then a else gcd(b, a % b)
        private val g = gcd(x, y)
        def numer = x / g
        def denom = y / g
        def + (r: Rational) = Rational(
          numer * r.denom + r.numer * denom,
          denom * r.denom)
        def - (r: Rational): Rational = ...
        def * (r: Rational): Rational = ...
        ...
      }

Operators for Rationals
=======================

This allows rational numbers to be used like `Int` or `Double`:

      val x = Rational(1, 2)
      val y = Rational(1, 3)
      x * x + y * y

Precedence Rules
================

The \red{precedence} of an operator is determined by its first character.

The following table lists the characters in increasing order of priority precedence:

        (all letters)
        |
        ^
        &
        < >
        = !
        :
        + -
        * / %
        (all other special characters)

Exercise
========

Provide a fully parenthesized version of

     a + b ^? c ?^ d less a ==> b | c

Every binary operation needs to be put into parentheses,
but the structure of the expression should not change.

